#include <stdio.h>
#define PerformanceCost 500
#define CostPerAttendee 3

int noOfAttendees(int ticket)
{
    return 120 - 20*(ticket-15)/5 ;
}

int income(int ticket)
{
    return ticket*noOfAttendees(ticket);
}

int cost(int ticket)
{
    return PerformanceCost + CostPerAttendee*noOfAttendees(ticket);
}

int profit(int ticket)
{
    return income(ticket)-cost(ticket);
}

int main()
{
    printf("Ticker Prices and profits are, \n\n");
    for(int i=5; i<50; i+=5)
    {
       if(profit(i)<0)
            printf("Ticket Price = Rs.%d/-\t------>\tProfit = Rs.%d/- (Loss) \n\n", i, profit(i));
       else
            printf("Ticket Price = Rs.%d/-\t------>\tProfit = Rs.%d/- \n\n", i, profit(i));
    }
    return 0;
}
